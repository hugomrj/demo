      
   
   
function main_inicio( obj ){    
    
    
    document.getElementById( obj.dom ).innerHTML =  "<article id=\"arti_cab\"></article><article id=\"arti_det\"></article>";
    

    // mostrar cabecera  
    ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html';    
    ajax.metodo = "GET";            
    document.getElementById( "arti_cab" ).innerHTML =  ajax.public.html();


    // inicializacion fechas en cabecera
    
        var fecha = new Date(); //Fecha actual
        var mes = fecha.getMonth()+1; //obteniendo mes
        var dia = fecha.getDate(); //obteniendo dia
        var ano = fecha.getFullYear(); //obteniendo año
        if(dia<10)
          dia='0'+dia; //agrega cero si el menor de 10
        if(mes<10)
          mes='0'+mes //agrega cero si el menor de 10
        var f =   ano+"-"+mes+"-"+dia;      
    
        var aperturadiaria_fecha = document.getElementById('aperturadiaria_fecha');    
        aperturadiaria_fecha.value  = f;
    
    
    
        aperturadiaria_fecha.onchange = function( ){
        
            apertura_diaria_inicio( aperturadiaria_fecha.value );
            
        };
        aperturadiaria_fecha.onchange();
 
        




}
   
   
   
   
   
function apertura_diaria_inicio( strfecha  ){    
    

        ajax.url = html.url.absolute()+'/api/aperturadiaria/existe?fecha='+strfecha ;
        ajax.metodo = "POST";        
        var res = ajax.private.json();    
        
        
        
        if (ajax.state == 200){
            //console.log(res);
            var ojson = JSON.parse(res) ;            
            var cabid = ojson['apertura'] ;
            document.getElementById('aperturadiaria_id').value = cabid  ;   
            
            apertura_diaria_detalles();
        }
        else{
            if (ajax.state == 204){                
                
                document.getElementById('aperturadiaria_id').value = 0  ;   

                apertura_diaria_proceso_iniciar_dia ();
            } 
        }
            


}   







function apertura_diaria_detalles(){    
    
    var obj = new AperturaDiaria();  
// mostrar detalle 
    ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html';    
    ajax.metodo = "GET";            
    document.getElementById( "arti_det" ).innerHTML =  ajax.public.html();


    // cargar tabla
    
        var aperturadiaria_id = document.getElementById( "aperturadiaria_id" ); 
    

        ajax.url = html.url.absolute()+"/api/aperturadiariadetalle/listan/"+aperturadiaria_id.value ; 

        ajax.metodo = "GET";        
        var json_root = ajax.private.json();    

        var ojson = JSON.parse(json_root) ;        
        var json = JSON.stringify(ojson['data'])
        
        tabla.json = json;    


        var detalles = new AperturaDiariaDetalle();          
        tabla.ini(detalles);
        tabla.gene();   
        tabla.formato(detalles);



        tabla.set.tablaid(detalles);     

        tabla.lista_registro(  selector_registro  );


      form_datos_lista_totales(json_root);

}   









function apertura_diaria_proceso_iniciar_dia (){    
    
    var obj = new AperturaDiaria();  
// mostrar detalle 
    ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/proceso_iniciar.html';    
    ajax.metodo = "GET";            
    document.getElementById( "arti_det" ).innerHTML =  ajax.public.html();



        var btn_iniciar_apertura = document.getElementById('btn_iniciar_apertura');
        btn_iniciar_apertura.addEventListener('click',
            function(event) {     


                    var strfecha = document.getElementById('aperturadiaria_fecha').value;    

                    loader.inicio();

                    var xhr =  new XMLHttpRequest();            
                    var url = html.url.absolute()+"/api/aperturadiaria/inicioapertura?fecha="+strfecha ; 
                    var metodo = "POST";     

                    xhr.open( metodo.toUpperCase(),   url,  true );      

                    xhr.onreadystatechange = function () {
                        if (this.readyState == 4 ){
                            loader.fin();

                            if (xhr.status == 200){

                                //ajax.headers.get();
                                ajax.local.token =  xhr.getResponseHeader("token") ;            
                                localStorage.setItem('token', xhr.getResponseHeader("token") );   
            
            
                                var aperturadiaria_fecha = document.getElementById('aperturadiaria_fecha');    
                                apertura_diaria_inicio( aperturadiaria_fecha.value );
                                
                                //apertura_diaria_detalles();
                                
                            }

                        }
                    };
                    xhr.onerror = function (e) {  
                      alert("error");
                    };         

                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    xhr.setRequestHeader("token", localStorage.getItem('token'));           

                    xhr.send( null );                                  


            },
            false
        );    





}   


    
    
function selector_registro ( linea ){
    
        ajax.url = html.url.absolute()+'/aplicacion/aperturadiariadetalle/htmf/form.html'; 
        ajax.metodo = "GET";       

        modal.ancho = 700;
        var obj = modal.ventana.mostrar("ide");   
        selector_modal_editdelete( obj, linea  );    
        
};





function selector_modal_editdelete ( obj, linea ){
    
    ajax.url = html.url.absolute()+'/api/aperturadiariadetalle/'+linea;    
    ajax.metodo = "GET";

    form.name = "form_aperturadiariadetalle" ;
    //form.json = ajax.private.json();   
    var datajson = ajax.private.json();      

    form.campos = ['selector_id'];
    form.disabled(true);
    form.disabled(false);

    //form.llenar();     
    var ojson = JSON.parse(datajson) ;
    
    document.getElementById( "aperturadiariadetalle_id" ).value = ojson['id'] ;
    document.getElementById( "aperturadiariadetalle_categoria" ).value 
            = ojson['producto']['categoria']['descripcion'] ;
    document.getElementById( "aperturadiariadetalle_producto" ).value 
            = ojson['producto']['nombre'] ;
    document.getElementById( "aperturadiariadetalle_precio_venta" ).value= fmtNum(ojson['precio_venta'] );
    
    
    document.getElementById( "aperturadiariadetalle_apertura_cantidad" ).value= fmtNum(ojson['apertura_cantidad'] );
        
    if (typeof ojson['cierre_cantidad'] == "undefined") {
        document.getElementById( "aperturadiariadetalle_cierre_cantidad" ).value= "";
    }
    else{
        document.getElementById( "aperturadiariadetalle_cierre_cantidad" ).value= fmtNum(ojson['cierre_cantidad'] );
    }
    
    
    
    if (typeof ojson['venta_cantidad'] == "undefined") {
        document.getElementById( "aperturadiariadetalle_venta_cantidad" ).value= "";
    }
    else{
        document.getElementById( "aperturadiariadetalle_venta_cantidad" ).value= fmtNum(ojson['venta_cantidad'] );
    }
    
    
    
    if (typeof ojson['monto_ingreso'] == "undefined") {
        document.getElementById( "aperturadiariadetalle_monto_ingreso" ).value= "";
    }
    else{
        document.getElementById( "aperturadiariadetalle_monto_ingreso" ).value= fmtNum(ojson['monto_ingreso'] );
    }
    
    
    
    /*
    
    document.getElementById( "aperturadiariadetalle_venta_cantidad" ).value= fmtNum(ojson['venta_cantidad'] );    
    document.getElementById( "aperturadiariadetalle_monto_ingreso" ).value= fmtNum(ojson['monto_ingreso'] );
    */



    document.getElementById( "aperturadiariadetalle_apertura_cantidad" ).disabled = false;    
    document.getElementById( "aperturadiariadetalle_cierre_cantidad" ).disabled = false;    
    
    
    
    
    document.getElementById('aperturadiariadetalle-acciones' ).innerHTML 
                    =  boton.basicform.get_botton_edit();

    
    
    
    var aperturadiariadetalle_apertura_cantidad = document.getElementById('aperturadiariadetalle_apertura_cantidad');          
    aperturadiariadetalle_apertura_cantidad.onblur  = function() {                
        aperturadiariadetalle_apertura_cantidad.value  = fmtNum(aperturadiariadetalle_apertura_cantidad.value);
    };     
    aperturadiariadetalle_apertura_cantidad.onblur();         
    
    
    
    
    
    var aperturadiariadetalle_cierre_cantidad = document.getElementById('aperturadiariadetalle_cierre_cantidad');          
    aperturadiariadetalle_cierre_cantidad.onblur  = function() {                

        if (aperturadiariadetalle_cierre_cantidad.value.toString().trim() != ""){
            aperturadiariadetalle_cierre_cantidad.value  = fmtNum(aperturadiariadetalle_cierre_cantidad.value);
        }
        
        
    };     
    aperturadiariadetalle_cierre_cantidad.onblur();         
    
    
    
    
    
    
    
    
    
    
    

    var btn__editar = document.getElementById( "btn__editar" );
    btn__editar.innerHTML = "Editar";
    btn__editar.addEventListener('click',
        function(event) {     
                
                
            var id = document.getElementById('aperturadiariadetalle_id').value;

                
            //if ( main_form_validar() )
            if ( true )
            {
            
                ajax.metodo = "put";


                ajax.url = html.url.absolute()+"/api/aperturadiariadetalle/"+id;                  
                
                form.name = "form_aperturadiariadetalle";


                var aperturadiariadetalle_apertura_cantidad = document.getElementById( "aperturadiariadetalle_apertura_cantidad" )
                var aperturadiariadetalle_cierre_cantidad = document.getElementById( "aperturadiariadetalle_cierre_cantidad" )
                var cierre_cant;
                if (aperturadiariadetalle_cierre_cantidad.value == ""){
                    cierre_cant = 'null';
                }
                else{
                    cierre_cant = NumQP(aperturadiariadetalle_cierre_cantidad.value) ;
                }
                


                var jdata = "{\"id\":" + id +","
                    +"\"apertura_cantidad\":"+ NumQP(aperturadiariadetalle_apertura_cantidad.value) +","    
                    +"\"cierre_cantidad\":"+ cierre_cant +"}"  ;  

console.log(jdata);

                var jsondata = ajax.private.json( jdata);                  
                
                if (ajax.state == 200){

                    btn__cancelar.click();
                    apertura_diaria_detalles();                        
                    msg.ok.mostrar("registro editado");                                   

                }
                else{
                    msg.error.mostrar("error de acceso");           
                } 
                
            }
        }
    ); 




    var btn__cancelar = document.getElementById( "btn__cancelar" );
    btn__cancelar.addEventListener('click',
        function(event) {     
                modal.ventana.cerrar(obj);
        }
    );    
   

};







function form_datos_lista_totales ( json_root ){
    

         //  suma
    
    
        var ojson = JSON.parse(json_root) ;         
        
            //var table = document.getElementById( "consulta-tabla" ).getElementsByTagName('tbody')[0];

        var suma_venta_cantidad = ojson['summary'][0]['suma_venta_cantidad']  ;               
        if (suma_venta_cantidad == null ){
            suma_venta_cantidad  = 0;
        }
        document.getElementById( "suma_venta_cantidad" ).innerHTML =  fmtNum(suma_venta_cantidad.toString());
        
   
    var suma_monto_ingreso = ojson['summary'][0]['suma_monto_ingreso']  ;               
        if (suma_monto_ingreso == null ){
            suma_monto_ingreso  = 0;
        }
        document.getElementById( "suma_monto_ingreso" ).innerHTML =  fmtNum(suma_monto_ingreso.toString());
        
    
}


    