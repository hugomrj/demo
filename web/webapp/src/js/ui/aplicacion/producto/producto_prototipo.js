

function Producto(){
    
   this.tipo = "producto";   
   this.recurso = "productos";   
   this.value = 0;
   this.form_descrip = "producto_nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   
   this.titulosin = "Producto"
   this.tituloplu = "Productos"   
      
   
   this.campoid=  'producto';
   this.tablacampos =  ['producto', 'nombre',  'categoria.descripcion'];
   
    this.etiquetas =  ['Codigo', 'Nombre', 'Categoria' ];                                  
    
    
    this.tablaformat = ['C', 'C', 'C'];                                  
   
   this.tbody_id = "vehiculo-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "producto-acciones";   
      
   
   this.parent = null;
   
    this.tabs =  ['Lista precio'];
   
   
    this.combobox = 
            {
                "categoria":{
                    "value":"categoria",
                    "inner":"descripcion"} ,       
                "porcentaje_iva":{
                    "value":"porcentaje_iva",
                    "inner":"descripcion"}                                             
            };   

   
   
   
}





Producto.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
};





Producto.prototype.form_ini = function() {    

    
};



Producto.prototype.form_validar = function() {    
    
   

    var producto_nombre = document.getElementById('producto_nombre');    
    if (producto_nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        producto_nombre.focus();
        producto_nombre.select();        
        return false;
    }       
        
    
    
    
    return true;
};







Producto.prototype.carga_combos = function( obj  ) {                
   
   
   
        var producto_categoria = document.getElementById("producto_categoria");
        var idedovalue = producto_categoria.value;
   
        ajax.url = html.url.absolute()+'/api/productoscategorias/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               
                
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['categoria'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                producto_categoria.appendChild(opt);                     
            }            
        }

    
    
            // porcentaje iva

            
        var producto_porcentaje_iva = document.getElementById("producto_porcentaje_iva");
        var idedovalue = producto_porcentaje_iva.value;
   
        ajax.url = html.url.absolute()+'/api/porcentajesiva/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();     
                
        var oJson = JSON.parse( datajson ) ;
        
        for ( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['porcentaje_iva'] );            
            
            //if (idedovalue != jsonvalue ){  
                
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                producto_porcentaje_iva.appendChild(opt);                     
            //}
            
        }            


            
        



};





Producto.prototype.tabuladores = function() {  
    
        
        var obj = new Producto();
        coleccion.ini(obj);
        
        document.getElementById( "producto-tabuladores" ).innerHTML =  coleccion.gene();    
    
    
        var producto = new Producto();
    
        var listaprecio = new ListaPrecioProducto(); 
        
        producto.value = document.getElementById('producto_producto').value;     
        
        listaprecio.cabecera = producto;          
        
        //listaprecio.tablacamposoculto = [0];    
        
        //var detalles = new AperturaDiariaDetalle();  
        //tabla.formato(listaprecio);
        
        coleccion.objetos = [ listaprecio ];                        
        coleccion.interaccion();     
        
};




