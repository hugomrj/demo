

function PrecioCategoria(){
    
   this.tipo = "preciocategoria";   
   this.recurso = "precioscategorias";   
   this.value = 0;
   
   this.form_descrip = "preciocategoria_descripcion_out";
   this.json_descrip = "descripcion";
   
   
   
   this.dom="";
   this.carpeta=  "/comercial";   
      
   
   
   this.titulosin = "Categoria de precio"
   this.tituloplu = "Categorias de precios"   
      
   this.campoid=  'precio_categoria';
   this.tablacampos =  ['precio_categoria', 'descripcion'];
   this.etiquetas =  ['categoria', 'Descripcion'];
   
   this.tbody_id = "categoriaprecio-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "categoriaprecio-acciones";   
   
   this.parent = null;
   
}







PrecioCategoria.prototype.new = function( obj  ) {                

    reflex.form(obj);
    reflex.acciones.button_add(obj);          
    
};






PrecioCategoria.prototype.form_validar = function() {    
    return true;
};




