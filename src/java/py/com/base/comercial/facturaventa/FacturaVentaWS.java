/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.comercial.facturaventa;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;


import py.com.base.comercial.facturaventadetalle.FacturaVentaDetalle;
import py.com.base.comercial.facturaventadetalle.FacturaVentaDetalleDAO;


@Path("facturasventas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class FacturaVentaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    FacturaVenta com = new FacturaVenta();       
                         
    public FacturaVentaWS() {
        
    }

    
    
    
    @GET    
    public Response list ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                
                autorizacion.actualizar();                                
                                
                
                FacturaVentaDAO dao = new FacturaVentaDAO();                
                List<FacturaVenta> lista = dao.list(page);                
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
    
    
    @GET           
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                                
                autorizacion.actualizar();                                
                
                FacturaVentaDAO dao = new FacturaVentaDAO();                
                List<FacturaVenta> lista = dao.search(page, q);
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
    
    
    
    
    
    
    
    @GET
    @Path("/{id}/detalle")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                 
        try 
        {                  
            
            
            if (autorizacion.verificar(strToken))          
            {               
                                
                
                autorizacion.actualizar();                
                                                
                FacturaVentaExt facturaventa = new FacturaVentaExt();         
                facturaventa = (FacturaVentaExt) persistencia.filtrarId(facturaventa, id );          

                
                FacturaVentaDetalleDAO fvd = new FacturaVentaDetalleDAO();
                List<FacturaVentaDetalle> detalles = fvd.factura(id);

                facturaventa.setDetalle(detalles);
                String json = gson.toJson( facturaventa );                       

                return Response
                        .status( Response.Status.OK )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();                           


            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();    
            }
        
        }     
                
        
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
    



 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
                     
        try {                    



            if (autorizacion.verificar(strToken))
            {                
                
                autorizacion.actualizar();    
                
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                FacturaVenta req = gson.fromJson(json, FacturaVenta.class);              

                                

                req.setUsuario( Integer.parseInt(autorizacion.token.getUser()) );
                this.com = (FacturaVenta) persistencia.insert(req);                                             


                List<FacturaVentaDetalle> det = req.getDetalle();   
                for (FacturaVentaDetalle fd : det) {                         
                    fd.setFactura(this.com.getFactura() );
                    persistencia.insert(fd);   
                }
        
                                
                
                FacturaVentaDAO dao = new FacturaVentaDAO();        
                FacturaVenta f = dao.update_monto( this.com.getFactura() );     
                
                
                /*
                TimbradoBoletaDAO timbradodao = new TimbradoBoletaDAO();        
                timbradodao.boleta_factura(
                        Integer.parseInt(autorizacion.token.getUser()) , 
                        this.facturaventa.getFactura() , 
                        req.getBoleta().getBoleta());                
                */
                
                
                if (this.com == null){
                    throw new Exception();                
                }
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                               
            }
        
        }     
        
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity( ex.getMessage() )
                    .header("token", autorizacion.encriptar() )
                    .build();                                        
        }        

    }    
 

    

        
    
        

    

    
}


