
package py.com.base.comercial.facturaventa;



import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

//import py.com.aplicacion.timbrado.Timbrado;

public class FacturaVentaSQL {
    
    public String list ( )
            throws Exception {
    
        String sql = "";            
                
        ReaderT reader = new ReaderT("FacturaVenta");        
        reader.fileExt = "lista.sql";        
        sql = reader.get( );            
        
        
        return sql ;             
    }        
    
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        
        
        ReaderT reader = new ReaderT("FacturaVenta");        
        reader.fileExt = "search.sql";        
        sql = reader.get(busqueda );                    
        
        
        return sql ;             
    }        
        
    
    
    public String  update_monto (Integer codigo ) 
            throws Exception {
    
        String sql = "";                                 
        
        
        ReaderT reader = new ReaderT("FacturaVenta");     
        reader.fileExt = "update_monto.sql";        
        sql = reader.get(codigo );    
        
        return sql ;             
    }        
        
        
    
    
}




