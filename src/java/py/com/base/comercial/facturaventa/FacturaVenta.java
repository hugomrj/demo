/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.facturaventa;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import py.com.base.comercial.cliente.Cliente;
//import py.com.aplicacion.timbrado_boleta.TimbradoBoleta;

import py.com.base.comercial.cliente.formapago.FormaPago;
import py.com.base.comercial.facturaventadetalle.FacturaVentaDetalle;


/**
 *
 * @author hugo
 */


public class FacturaVenta {
    
    private Integer factura;
    private String factura_numero;
    
    private Date fecha_factura;        
    private Cliente cliente ;
    private FormaPago forma_pago ;
    
    private Long gravada0 ;
    private Long gravada5 ;
    private Long gravada10 ;
    private Long iva5 ;
    private Long iva10 ;
    private Long total_iva ;
    private Long monto_total ;
    
    private Long importe_pagado ;
    private Long importe_vuelto ;
    
    private Integer usuario ;    
    private String estado;
    
    private Integer emisor;
    
    
    private List<FacturaVentaDetalle> detalle= new ArrayList<>();    
    
    
    
    public Date getFecha_factura() {
        return fecha_factura;
    }

    public void setFecha_factura(Date fecha_factura) {
        this.fecha_factura = fecha_factura;
    }



    public Long getGravada0() {
        return gravada0;
    }

    public void setGravada0(Long gravada0) {
        this.gravada0 = gravada0;
    }

    public Long getGravada5() {
        return gravada5;
    }

    public void setGravada5(Long gravada5) {
        this.gravada5 = gravada5;
    }

    public Long getGravada10() {
        return gravada10;
    }

    public void setGravada10(Long gravada10) {
        this.gravada10 = gravada10;
    }

    public Long getIva5() {
        return iva5;
    }

    public void setIva5(Long iva5) {
        this.iva5 = iva5;
    }

    public Long getIva10() {
        return iva10;
    }

    public void setIva10(Long iva10) {
        this.iva10 = iva10;
    }

    public Long getTotal_iva() {
        return total_iva;
    }

    public void setTotal_iva(Long total_iva) {
        this.total_iva = total_iva;
    }

    public Long getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(Long monto_total) {
        this.monto_total = monto_total;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Integer getFactura() {
        return factura;
    }

    public void setFactura(Integer factura) {
        this.factura = factura;
    }




    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



    public Integer getEmisor() {
        return emisor;
    }

    public void setEmisor(Integer emisor) {
        this.emisor = emisor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public FormaPago getForma_pago() {
        return forma_pago;
    }

    public void setForma_pago(FormaPago forma_pago) {
        this.forma_pago = forma_pago;
    }


    public String getFactura_numero() {
        return factura_numero;
    }

    public void setFactura_numero(String factura_numero) {
        this.factura_numero = factura_numero;
    }

    public List<FacturaVentaDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<FacturaVentaDetalle> detalle) {
        this.detalle = detalle;
    }

    public Long getImporte_pagado() {
        return importe_pagado;
    }

    public void setImporte_pagado(Long importe_pagado) {
        this.importe_pagado = importe_pagado;
    }

    public Long getImporte_vuelto() {
        return importe_vuelto;
    }

    public void setImporte_vuelto(Long importe_vuelto) {
        this.importe_vuelto = importe_vuelto;
    }

    
}



