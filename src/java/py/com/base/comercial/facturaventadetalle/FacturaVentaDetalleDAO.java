/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.facturaventadetalle;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


public class FacturaVentaDetalleDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public FacturaVentaDetalleDAO ( ) throws IOException  {
    }
      
    
    public List<FacturaVentaDetalle>  factura (Integer numero) {
                
        List<FacturaVentaDetalle>  lista = null;        
        try {                        
                        
            FacturaVentaDetalleRS rs = new FacturaVentaDetalleRS();            
            
            lista = new Coleccion<FacturaVentaDetalle>().resultsetToList(
                    new  FacturaVentaDetalle(), rs.factura(numero)
            );                        
            this.total_registros = rs.total_registros  ;            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
              
    
        
}
