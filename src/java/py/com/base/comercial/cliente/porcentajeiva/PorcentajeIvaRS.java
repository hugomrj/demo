/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente.porcentajeiva;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.xml.Global;


public class PorcentajeIvaRS  {
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
    
    public PorcentajeIvaRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }

    
    public ResultSet  all (  ) throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = " SELECT porcentaje_iva, descripcion\n " +
                                " FROM aplicacion.porcentajes_iva\n " +
                                " order by porcentaje_iva desc " ;

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
        
    
    
    
}
