/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente.porcentajeiva;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


public class PorcentajeIvaDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public PorcentajeIvaDAO ( ) throws IOException  {
    }
      
      

    public List<PorcentajeIva>  all () {
                
        List<PorcentajeIva>  lista = null;        
        try {                        
                        
            PorcentajeIvaRS rs = new PorcentajeIvaRS();            
            lista = new Coleccion<PorcentajeIva>().resultsetToList(
                    new PorcentajeIva(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      


}
