/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.comercial.cliente;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

import jakarta.ws.rs.QueryParam;

import jakarta.ws.rs.core.MediaType;

import jakarta.ws.rs.core.Response;

import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;


/**
 * REST Web Service
 * @author hugo
 */


@Path("clientes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class ClienteWS {

    @Context
    private UriInfo context;
    private Persistencia persistencia = new Persistencia();
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;

    Cliente com = new Cliente();



    public ClienteWS() {


      

    }




    @GET
    public Response list (
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {

            if (page == null) {
                page = 1;
            }

        try {

            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                ClienteDAO dao = new ClienteDAO();
                List<ClienteExt> lista = dao.list(page);
                String json = gson.toJson( lista );

                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();
            }
            else{

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();
        }
    }





    @GET
    @Path("/search/")
    public Response search (
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,
            @MatrixParam("q") String q
            ) {


            if (page == null) {
                page = 1;
            }
            if (q == null){
                q = "";
            }

        try {

            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                ClienteDAO dao = new ClienteDAO();

                List<ClienteExt> lista = dao.search(page, q);
                String json = gson.toJson( lista );

                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();
        }
    }






    @GET
    @Path("/{id}")
    public Response get(
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {

        try
        {
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();
                this.com  = (Cliente) persistencia.filtrarId(this.com, id);

                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }


                String json = gson.toJson(this.com);
                
                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }

        }
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();
        }
    }






    @GET
    @Path("/identificador/{identi}")
    public Response identificador(
            @HeaderParam("token") String strToken,
            @PathParam ("identi") String identi ) {

        try
        {
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                this.com  = (Cliente) new ClienteDAO().identificador(identi);

                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }

                
                String json = gson.toJson(this.com);
                
                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }

        }
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();
        }
    }






    @POST
    public Response add(
            @HeaderParam("token") String strToken,
            String json ) {

        try {

            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                Cliente req = gson.fromJson(json, this.com.getClass());
                
                this.com = (Cliente) persistencia.insert(req);

                if (this.com == null){
                    throw new Exception();
                }

                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();

            }

        }
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();
        }

    }







    @PUT
    @Path("/{id}")
    public Response edit (
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json
            ) {

        try {

            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                Cliente req = new Gson().fromJson(json, Cliente.class);
                req.setCliente(id);

                this.com = (Cliente) persistencia.update(req);

                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();
            }
            else{

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();

            }

        }
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();

        }
    }






    @DELETE
    @Path("/{id}")
    public Response delete (
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {

        try {

            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;


                if (filas != 0){

                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();
                }
                else{

                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();


                }
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }
        }

        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();
        }

    }








    @GET
    @Path("/{doc}/{identificador}/{digito}")
    public Response getClienteDocNumero (
            @HeaderParam("token") String strToken,
            @PathParam ("doc") String doc,
            @PathParam ("identificador") String identificador,
            @PathParam ("digito") String digito
            ) {

        try
        {
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();

                Integer intDigito = 0;
                intDigito = intDigito.valueOf(digito);

                this.com  = (Cliente) new ClienteDAO().getClienteDocNumero(doc, identificador, intDigito);


                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                String json = gson.toJson(this.com);

                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();
            }

        }
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();
        }
    }









}
