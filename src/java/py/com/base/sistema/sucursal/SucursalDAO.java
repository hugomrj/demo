/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.sistema.sucursal;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


/**
 *
 * @author hugom_000
 */

public class SucursalDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public SucursalDAO ( ) throws IOException  {
    }
      
    
    public List<Sucursal>  list (Integer page) {
                
        List<Sucursal>  lista = null;        
        try {                        
                        
            SucursalRS rs = new SucursalRS();            
            lista = new Coleccion<Sucursal>().resultsetToList(
                    new Sucursal(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<Sucursal>  search (Integer page, String busqueda) {
                
        List<Sucursal>  lista = null;
        
        try {                       
                        
            SucursalRS rs = new SucursalRS();
            lista = new Coleccion<Sucursal>().resultsetToList(
                    new Sucursal(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
    
    
    

    public List<Sucursal>  all () {
                
       
        List<Sucursal>  lista = null;        
        try {                        
                        
            SucursalRS rs = new SucursalRS();            
            lista = new Coleccion<Sucursal>().resultsetToList(
                    new Sucursal(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            
System.out.println("ERRR dao all");            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
          
        
}
