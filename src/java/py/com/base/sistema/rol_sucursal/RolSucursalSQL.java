
package py.com.base.sistema.rol_sucursal;



import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

public class RolSucursalSQL {
    


    public String cabRol ( Integer rol )
            throws Exception {
    
        String sql = "";            

        //ReaderSQL readerSQL = new ReaderSQL("RolSucursal");
        ReaderT readerSQL = new ReaderT("RolSucursal");
        readerSQL.fileExt = "cabRol.sql";
        sql = readerSQL.get( rol );     
                
        return sql ;                     
        
    }        


    
    

    public String cabSucursal ( Integer sucursal )
            throws Exception {
    
        String sql = "";            
        
        ReaderT readerSQL = new ReaderT("RolSucursal");
        readerSQL.fileExt = "cabSucursal.sql";
        sql = readerSQL.get( sucursal );   
        
        
   
        return sql ;             
    }        


    
    
}




