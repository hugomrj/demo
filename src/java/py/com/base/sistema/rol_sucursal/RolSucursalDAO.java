/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.sistema.rol_sucursal;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


/**
 *
 * @author hugom_000
 */

public class RolSucursalDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public RolSucursalDAO ( ) throws IOException  {
    }
      
    public List<RolSucursal>  cabRol (Integer rol, Integer page) {
                
        List<RolSucursal>  lista = null;        
        try {          
            RolSucursalRS rs = new RolSucursalRS();      
            
            lista = new Coleccion<RolSucursal>().resultsetToList(
                    new RolSucursal(),
                    rs.cabRol(rol, page)
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    

    public List<RolSucursal>  cabSucursal (Integer sucursal, Integer page) {
                
        List<RolSucursal>  lista = null;        
        try {          
            RolSucursalRS rs = new RolSucursalRS();      
            
            lista = new Coleccion<RolSucursal>().resultsetToList(
                    new RolSucursal(),
                    rs.cabSucursal(sucursal, page)
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
    
        
}
