/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.preciocategoria;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class PrecioCategoriaSQL {
    
    
    
    public String all (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("PrecioCategoria");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
    
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new PrecioCategoria(), busqueda);        
        
        return sql ;             
    }        
        
    
    
    
}
