/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.producto;

import py.com.base.aplicacion.productocategoria.ProductoCategoria;
import py.com.base.comercial.cliente.porcentajeiva.PorcentajeIva;


/**
 *
 * @author hugom_000
 */
public class Producto {
    
    private Integer producto;    
    private String codigo;
    private String codigo_barras;
    private String nombre;    
    private ProductoCategoria categoria;
    private PorcentajeIva porcentaje_iva;
    

    
    public Integer getProducto() {
        return producto;
    }

    public void setProducto(Integer producto) {
        this.producto = producto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ProductoCategoria getCategoria() {
        return categoria;
    }

    public void setCategoria(ProductoCategoria categoria) {
        this.categoria = categoria;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo_barras() {
        return codigo_barras;
    }

    public void setCodigo_barras(String codigo_barras) {
        this.codigo_barras = codigo_barras;
    }

    public PorcentajeIva getPorcentaje_iva() {
        return porcentaje_iva;
    }

    public void setPorcentaje_iva(PorcentajeIva porcentaje_iva) {
        this.porcentaje_iva = porcentaje_iva;
    }

    
}

