/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.producto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Map;
import nebuleuse.ORM.JsonObjeto;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class ProductoJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ProductoJSON ( ) throws IOException  {
    
    }
      
    
    
    


    public JsonElement  producto_preciocategoria (Integer producto,
            Integer precio_categoria ) {
        
        Map<String, String> map = null;        
        //JsonArray jsonarray = new JsonArray();      
        JsonElement jsonElement = null  ;
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
        
        try 
        {   
            
            ProductoRS rs = new ProductoRS();            
            ResultSet resulset = rs.producto_preciocategoria(  producto, precio_categoria );                
            
            if(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                jsonElement = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
          //      jsonarray.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonElement ;         
        }
    }      
          
        
    
    

    public JsonObject  lista ( Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();
        
        try 
        {   
            
            ResultadoSet resSet = new ResultadoSet();                   
            String sql = SentenciaSQL.select(new Producto());       
            
            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            
            JsonObject jsonPaginacion = new JsonObject();
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            //JsonArray jsonarrayPaginacion = new JsonArray();            
            //jsonarrayPaginacion = new JsonObjeto().array_paginacion(sql, page);
            
            
//System.out.println(jsonarrayPaginacion);
            
            // union de partes
            //jsonObject.add("paginacion", jsonarrayPaginacion);
            jsonObject.add("paginacion", jsonPaginacion);
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     

   
    


        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
            
    
    
    
        
}
