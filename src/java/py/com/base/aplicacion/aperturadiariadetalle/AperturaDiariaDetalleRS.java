/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiariadetalle;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import nebuleuse.ORM.RegistroMap;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class AperturaDiariaDetalleRS {

    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
    
    public AperturaDiariaDetalleRS ( ) throws IOException, SQLException  {
              
    }
   
   
    public void inicio ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();
    }
   
    
    
    
    
    public ResultSet  lista ( Integer apertura ) throws Exception {

            this.inicio();
        
            statement = conexion.getConexion().createStatement();      
            
            String sql = new AperturaDiariaDetalleSQL().lista(apertura);
                    
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    

    
    public ResultSet  listan_data (  Integer apertura ) 
            throws Exception {

        this.inicio();          
        
        String sql = new AperturaDiariaDetalleSQL().listan_data( apertura);
        
        
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
    
    
    public ResultSet  listan_sum (  Integer apertura ) 
            throws Exception {

        this.inicio();          
        
        String sql = new AperturaDiariaDetalleSQL().listan_sum( apertura);
        
        
        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                             
    }
    
    
    


    
}
