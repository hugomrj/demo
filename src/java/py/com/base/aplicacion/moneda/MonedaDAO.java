/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.moneda;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class MonedaDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public MonedaDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Moneda>  all () {
                
        List<Moneda>  lista = null;        
        try {                        
                        
            MonedaRS rs = new MonedaRS();            
            lista = new Coleccion<Moneda>().resultsetToList(
                    new Moneda(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
