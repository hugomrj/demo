/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.aplicacion.lista_precio_producto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;






@Path("listaspreciosproductos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class ListaPrecioProductoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    
    ListaPrecioProducto obj = new ListaPrecioProducto();       
    
    
                         
    public ListaPrecioProductoWS() {
    }





    @GET           
    @Path("/producto/{producto}/")
    public Response cabProducto ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,   
            @PathParam ("producto") Integer producto
            ) {
        
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))            
            {                
                autorizacion.actualizar();                                
                
                
                ListaPrecioProductoDAO dao = new ListaPrecioProductoDAO();                
                List<ListaPrecioProducto> lista = dao.cabProducto(producto, page);
                String json = gson.toJson( lista );     
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();  
            }        
        }    
        
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }                    
        
    }    
    
    
    
    


 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
                     
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                ListaPrecioProducto req = gson.fromJson(json,  this.obj.getClass());                   
                
                this.obj =  (ListaPrecioProducto) persistencia.insert(req);
                
                
                if (this.obj == null){
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity("null")
                            .header("token", autorizacion.encriptar())
                            .build();                          
                    
                }
                else
                {

                    json = gson.toJson(this.obj);
                            
                    return Response
                            .status(Response.Status.OK)
                            .entity(json)
                            .header("token", autorizacion.encriptar())
                            .build();                         
                    
                }                
                
  
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();    
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        
    
    
    
      
    
        
    
    
    
    

    
}