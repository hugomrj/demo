/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.lista_precio_producto;

import py.com.base.aplicacion.preciocategoria.PrecioCategoria;
import py.com.base.aplicacion.producto.Producto;


public class ListaPrecioProducto {

    private Integer lista_precio;
    private Producto producto;
    private PrecioCategoria precio_categoria;
    private Long monto;

    public Integer getLista_precio() {
        return lista_precio;
    }

    public void setLista_precio(Integer lista_precio) {
        this.lista_precio = lista_precio;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public PrecioCategoria getPrecio_categoria() {
        return precio_categoria;
    }

    public void setPrecio_categoria(PrecioCategoria precio_categoria) {
        this.precio_categoria = precio_categoria;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }



    
}


