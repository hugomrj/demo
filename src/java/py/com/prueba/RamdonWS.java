/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.prueba;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;



/**
 * REST Web Service
 * @author hugo
 */


@Path("ramdon")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class RamdonWS {

    @Context
    private UriInfo context;    
    
    public RamdonWS() {
    }

    
    
    
    
    
    @GET
    @Path("/aleatorio")
    public Response get( ) {
    
        
        double u = Math.random()*1000;
        int i = (int) u;               
        
        
                
        System.out.println("ramdon:  " + i);
        
        
        return Response                
                .status(Response.Status.OK)
                .entity(String.valueOf(i))                
                .build();       

        
    }    
      
        
        


 
    
}