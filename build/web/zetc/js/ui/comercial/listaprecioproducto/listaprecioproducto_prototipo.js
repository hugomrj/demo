

function ListaPrecioProducto(){
        
    this.tipo = "listaprecioproducto";   
    this.recurso = "listaspreciosproductos";   
    this.value = 0;

   this.carpeta=  "/comercial";   


    this.campoid=  'lista_precio';
    this.tablacampos = ['lista_precio', 'precio_categoria.descripcion' , 'producto.nombre', 'monto'  ];    
    
    this.etiquetas =  [ 'lista', 'categoria precio', 'producto', 'monto'];
    
    this.tablaformat = [ 'C', 'C', 'C', 'N' ];      
    
    
    this.tablacamposoculto = [];    
    
    this.formcampooculto = [];     
        
    this.titulosin = ""
    this.tituloplu = ""  
    
    this.botones_lista = "acciones-lista";
    this.botones_form = "listaprecioproducto-acciones";    
    
        
    this.funciones = null;  
    this.venactiva = null;

    this.cabecera= "";    
    //this.foreign = [ new Usuario(), new Rol() ] ;
    
    this.break = false;    
};




ListaPrecioProducto.prototype.sublista = function( obj ) {          
    
  
  
   if ( obj.break  == false )
   {
        tabla.setObjeto(obj);
        
        obj.tablacamposoculto = [0];    
        
        coleccion.sublista( obj );     
        
        tabla.formato(obj);
        
        
   }

    
};





ListaPrecioProducto.prototype.modal_new = function(  obj  ) {    


    modal.ancho = 700;

    modal.form.new( obj );  
    
    
    obj.foreign = [ new Producto(), new PrecioCategoria() ] ;
    
    reflex.ui.foreign_more(obj);
    
    reflex.ui.foreign_decrip(obj);

    modal.form.pasarcab(obj);
    
    modal.form.ocultarcab(obj);
    
    var caid = document.getElementById( "listaprecioproducto_lista_precio");
    caid.value = 0;    
    caid.disabled=true;
    

    //this.form_ini();
};






ListaPrecioProducto.prototype.form_validar = function( objeto ) {    



    
    var listaprecioproducto_monto = document.getElementById('listaprecioproducto_monto');        
    if (parseInt(NumQP(listaprecioproducto_monto.value)) <= 0 )         
    {
        msg.error.mostrar("Falta monto");    
        listaprecioproducto_monto.focus();
        listaprecioproducto_monto.select();                
        return false;
    }        
       
    
    
    return true;
}





ListaPrecioProducto.prototype.form_ini = function() {    

    
    var listaprecioproducto_monto = document.getElementById('listaprecioproducto_monto');          
    listaprecioproducto_monto.onblur  = function() {                
        listaprecioproducto_monto.value  = fmtNum(listaprecioproducto_monto.value);
    };     
    listaprecioproducto_monto.onblur();         
    
    
};







