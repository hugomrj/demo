
function FacturaVenta(){
    
   this.tipo = "facturaventa";   
   this.recurso = "facturasventas";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Factura de venta";
   this.tituloplu = "Facturas de ventas";      
    
   this.campoid=  'factura';
   this.tablacampos =  [ 'fecha_factura', 'factura', 'cliente.nombre',
                            'monto_total', 'forma_pago.descripcion'];   
   
   
   
   this.tablaformat =  ['D',  'N' , 'C',  'N' , 'C'];               
   
   
    this.etiquetas =  [ 'Fecha', 'Numero', 
                                'Cliente',
                            'Total', 'Forma pago'];   
   
   
   
   
   this.tbody_id = "facturaventa-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "facturaventa-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   
   
}





FacturaVenta.prototype.lista_new = function( obj  ) {                


    facturaventa_form(obj);     
    obj.carga_combos( obj );
    facturaventa_generar_nuevo( obj );
    
    
    
        var fvd = new FacturaVentaDetalle();   
        fvd.new(fvd);    
    
        boton.ini(obj);
        document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                =  boton.basicform.get_botton_pay_cancel();   
                    
        facturaventa_form_add (  obj  ) ;
        
        // generar variable de session para detalle de factura
        //localStorage.setItem('fvdls', "");     
        var fvdls = "";
        
        localStorage.setItem('fvdls', fvdls);     
        
        
        var ftr1 = document.getElementById( "ftr1" );
        ftr1.style.display = "none";
            
        var ftr3 = document.getElementById( "ftr3" );
        ftr3.style.display = "none";            
        
        
        
        

        var fecha = new Date(); //Fecha actual
        var mes = fecha.getMonth()+1; //obteniendo mes
        var dia = fecha.getDate(); //obteniendo dia
        var ano = fecha.getFullYear(); //obteniendo año
        if(dia<10)
          dia='0'+dia; //agrega cero si el menor de 10
        if(mes<10)
          mes='0'+mes //agrega cero si el menor de 10
        var f =   ano+"-"+mes+"-"+dia;    
        
        var facturaventa_fecha_factura = document.getElementById( "facturaventa_fecha_factura" );
        facturaventa_fecha_factura.value = f;
    
        
        
        
        
    
};







FacturaVenta.prototype.form_validar = function() {   
    
    
    
    var facturaventa_fecha_factura = document.getElementById('facturaventa_fecha_factura');
    if (facturaventa_fecha_factura.value == ""){
        msg.error.mostrar("Error en fecha");                    
        facturaventa_fecha_factura.focus();
        facturaventa_fecha_factura.select();                                       
        return false;        
    }
    
    
    // si no existen detalles
    var fvdls = localStorage.getItem('fvdls');
    if (fvdls.toString().trim() == ''){
        msg.error.mostrar("No existen registros en detalle de factura");    
        return false;
    }

    
    var facturaventa_cliente_descrip = document.getElementById('facturaventa_cliente_descrip');
    if (facturaventa_cliente_descrip.value == ""){
        msg.error.mostrar("Falta agregar cliente");                    
        return false;        
    }
    
    
    
    return true;
};





FacturaVenta.prototype.form_ini = function() {    
    
    
};







FacturaVenta.prototype.carga_combos = function( obj  ) {                
    
    // cargar combo
        var facturaventa_forma_pago = document.getElementById(   "facturaventa_forma_pago" );
   
        ajax.url = html.url.absolute()+'/api/formapagos/all' ;
        
        ajax.metodo = "GET";   
        var fv_json = ajax.private.json();               
        var oJson = JSON.parse( fv_json ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var opt = document.createElement('option');                        
            opt.value = oJson[x]['forma_pago'];
            opt.innerHTML = oJson[x]['descripcion'];                        
            facturaventa_forma_pago.appendChild(opt);                     
            
        }
        
};


