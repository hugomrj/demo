
SELECT id, 
productos_categorias.descripcion catgoria_descripcion, 
productos.nombre producto_nombre, 
apertura_diaria_detalle.producto, apertura_diaria_detalle.precio_venta, apertura_cantidad,  
(apertura_cantidad - tventas.suma_cantidad) as cierre_cantidad, 
tventas.suma_cantidad as venta_cantidad, 
tventas.suma_ventas as monto_ingreso, 
apertura_diaria.apertura, apertura_diaria.fecha 
FROM aplicacion.apertura_diaria_detalle inner join aplicacion.productos  
on (apertura_diaria_detalle.producto = productos.producto)  
inner join aplicacion.productos_categorias  
on (productos_categorias.categoria  = productos.categoria) 
inner join aplicacion.apertura_diaria 
on (apertura_diaria.apertura = apertura_diaria_detalle.apertura) 
left join  
( 
SELECT  factura_ventas.fecha_factura, producto, sum(cantidad) suma_cantidad, sum(sub_total) suma_ventas 
FROM aplicacion.factura_ventas_detalle  
inner join aplicacion.factura_ventas on (factura_ventas.factura = factura_ventas_detalle.factura ) 
group by factura_ventas.fecha_factura, producto 
) as tventas 
on (apertura_diaria.fecha = tventas.fecha_factura  
and apertura_diaria_detalle.producto = tventas.producto 
) 
where apertura_diaria.apertura = v0   
order by productos.categoria, producto  
 
